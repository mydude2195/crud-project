import React, { Component } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import './dist/react-bootstrap-table-all.min.css'
import axios from 'axios'

class App extends Component {
constructor() {
super();
this.state = {
  items:[]
  
  };
  this.Update = this.Update.bind(this)
}


async fetchRecords() {
    const url = "http://178.128.196.163:3000/api/records/";
    const response = await fetch(url)
    const data = await response.json()
    
    this.setState({
      data: data.map((item, i) => ({id: item._id,
        name: item.data.name,
        age: item.data.age   
    }))
    })
    console.log(data)}

    componentDidMount()
    {this.fetchRecords() }

    handleChange = event => {
      this.setState({ id: event.target.value });
    }
  
    handleSubmit = event => {
      event.preventDefault();
      
      axios.delete(`http://178.128.196.163:3000/api/records/${this.state.id}`)
        .then(res => {
          this.fetchRecords();
          console.log(res)
          console.log(res.data);
        })}
        

  updateRecords() {
      
        console.log(this.state)
        axios.put('http://178.128.196.163:3000/api/records/', {data:{
         name: this.state.name,
         age: this.state.age }})
             .then(res => {
               console.log(res)
             })}

Update(row){
  axios.post('http://178.128.196.163:3000/api/records/' + row.id, {data:{
      name: row.name,
      age: row.age
  }})
      .then(response => {
          console.log(response);
      })
      .catch(error => {
          console.log(error);
      });
}

changeHandler = (e) => {
  this.setState({
    [e.target.name]:e.target.value
  })
}
submitHandler = e => {
  e.preventDefault();
    console.log(this.state)
    axios.put('http://178.128.196.163:3000/api/records/', {data:{
     name: this.state.name,
     age: this.state.age }})
         .then(res => this.fetchRecords())}

      

render() {
  console.log(this.state)
  const { name, age } = this.state
  const styles = {
    container:{
      backgroundColor: '#1ba9ff',
      textAlign: 'center',
      width: 175.5,
       height: 30,
       borderRadius: 3,
       borderColor: '#000000',
       paddingTop:5,
       paddingBottom:30,
       fontSize: 18,
       flex: 1
       

    }     
  }
  const cellEditProp = {
    mode: 'click',
    afterSaveCell: row=>this.Update(row)
};
  const Bstyles = {
    container: {
     backgroundColor: '#ff5e18',
     textAlign: 'center',
     width: 120,
     height: 30,
     borderRadius: 3,
     borderColor: '#000000',
     borderWidth: 2,
     flexDirection: 'row'

    }
  }

return ( 

<div >
  <form onSubmit={this.submitHandler}>
<div >
  <label>
    <h4>Name:</h4>
  <input type='text' name='name' value={name} onChange={this.changeHandler} />
  </label>
</div>
<div>
  <label>
  <h4> Age:</h4>
  <input type='text' name='age' value={age} onChange={this.changeHandler} />
  </label>
</div>

<button className="button" style={styles.container} type="submit" >Submit</button>
  </form>
  <div >
        <form onSubmit={this.handleSubmit}>
          <label>
          <h4>Person ID:</h4>
            <input type="text" name="id" onChange={this.handleChange} />
          </label>
          <button className="button" style={Bstyles.container} type="submit">Delete</button>
        </form>
      </div>
  
  

  <BootstrapTable striped bordered hover 
    data={this.state.data}
    cellEdit={cellEditProp}
    
    >
      
      <TableHeaderColumn isKey dataField='id'>ID</TableHeaderColumn>
      <TableHeaderColumn  editable={true} dataField='name'>Name</TableHeaderColumn>
      <TableHeaderColumn  editable={true} dataField='age'>Age</TableHeaderColumn>

  </BootstrapTable>
  
  
  </div>

);
}}
export default App;
